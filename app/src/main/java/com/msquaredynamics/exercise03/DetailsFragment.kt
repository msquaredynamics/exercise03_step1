package com.msquaredynamics.exercise03

import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_details.view.*



class DetailsFragment : Fragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.d("DetailsFragment", "Fragment created ${this}")
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val mView = inflater.inflate(R.layout.fragment_details, container, false)

        val articleId = arguments?.getInt("articleId", 0)

        val article = articles.find {
            it.id == articleId
        }

        mView.textview_frag_details_title.text = article?.title
        mView.textview_frag_details_description.text = article?.description
        return mView
    }


    override fun onDestroy() {
        super.onDestroy()
        Log.d("DetailsFragment", "Fragment destroyed ${this}")
    }

}
