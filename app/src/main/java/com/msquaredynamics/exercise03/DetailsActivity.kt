package com.msquaredynamics.exercise03

import android.support.v7.app.AppCompatActivity
import android.os.Bundle

class DetailsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        initialize_1(savedInstanceState)
        //initialize_2(savedInstanceState)
        //initialize_3(savedInstanceState)

        /**
         * setContentView is not called here because we don't need to inflate a layout for the activity. The layout
         * will be inflated directly by the DetailsFragment instance.
         */

    }


    /**
     * This method will setup correctly the view hierarchy, because a new fragment is added only the first time the
     * activity is created. Changing the screen orientation won't add new fragments.
     */
    private fun initialize_1(savedInstanceState: Bundle?) {

        if (savedInstanceState == null) {
            val details = DetailsFragment().apply {
                arguments = intent.extras
            }

            /*
             * android.R.id.content is always the root view of the layout. It is a handy way to access the layout without
             * knowing its ID.
             */
            supportFragmentManager.beginTransaction()
                .replace(android.R.id.content, details)
                .commit()
        }
    }


    /**
     * This method will create an overhead in the view hierarchy, because a new fragment is added every time the
     * activity is created. Changing the screen orientation will add new fragments. Use the Layout Inspector tool
     * to see how multiple layouts are created
     */
    private fun initialize_2(savedInstanceState: Bundle?) {
        val details = DetailsFragment().apply {
            arguments = intent.extras
        }

        /*
         * android.R.id.content is always the root view of the layout. It is a handy way to access the layout without
         * knowing its ID.
         */
        supportFragmentManager.beginTransaction()
            .add(android.R.id.content, details)
            .commit()

    }



    /**
     * This method will NOT create the overhead in the view hierarchy, because a new fragment replaces the old one when
     * the activity is created. Use the Layout Inspector tool to see that the view hierarchy is correct
     *
     ******* HOWEVER, this is still inefficient! ******
     *
     * When the screen orientation changes, the activity is destroyed and the same happens to the fragments hosted by it.
     * Therefore, the DetailFragment actually shown is destroyed. When the system finishes changing the screen orientation,
     * it will recreate the view hierarchy and will recreate a new DetailsFragment passing the previous state to its
     * onCreate method. Here we create a new fragment and replace the one the system has just created!
     *
     * This can be seen looking at the Log. Every time the DetailsFragment is created or destroyed, the Log shows it as:
     * Fragment created DetailsFragment{....} or Fragment destroyed DetailsFragment{....}
     * Look at the memory addresses of the created/destroyed fragments and it will be clear how a fragment is created by
     * the system and destroyed right after it because it's replaced by the transaction.
     *
     * Looking at the Log when invoking initialize_1() won't show this behaviour
     *
     */
    private fun initialize_3(savedInstanceState: Bundle?) {
        val details = DetailsFragment().apply {
            arguments = intent.extras
        }

        /*
         * android.R.id.content is always the root view of the layout. It is a handy way to access the layout without
         * knowing its ID.
         */
        supportFragmentManager.beginTransaction()
            .replace(android.R.id.content, details)
            .commit()

    }
}
