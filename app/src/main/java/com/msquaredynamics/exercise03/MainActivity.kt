package com.msquaredynamics.exercise03

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle


data class Article(var id: Int, var title: String, var description: String)

val articles = listOf(
    Article(0, "Title01", "Description of title 01"),
    Article(1, "Title02","Description of title 02"),
    Article(2, "Title03","Description of title 03")
)


class MainActivity : AppCompatActivity(), TitlesFragment.OnArticleSelectedListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }


    override fun onArticleSelected(articleId: Int) {
        val mIntent = Intent(this, DetailsActivity::class.java).apply {
            putExtra("articleId", articleId)
        }

        startActivity(mIntent)
    }
}
