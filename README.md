# Exercise03_step1

Step 1 of the Exercise03. This version displays a (hardcoded, fixed) list of Articles, and allowes the user to click on a title.
When a title is selected, it starts a new activity that displays the title and description of the selected title.
In this version (step1) only the portrait mode is handled.

